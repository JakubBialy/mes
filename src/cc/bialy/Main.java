package cc.bialy;

import org.tc33.jheatchart.HeatChart;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cc.bialy.utils.Utils;

public class Main {
	public static List<Element> elements = new ArrayList<>();
	public static List<Node> nodes = new ArrayList<>();
	public static Matrix globalH;
	public static Matrix globalC;
	public static Matrix globalP;

	public static void main(String[] args) {
		init();
		solver();
	}

	public static Matrix getT0() {
		double temp[] = new double[nodes.size()];

		for (int i = 0; i < nodes.size(); i++) {
			temp[i] = nodes.get(i).getTemperature();
		}

		return new Matrix(temp, nodes.size(), 1);
	}

	private static void solver() {
		Matrix cdt = globalC.multiply(1 / GlobalData.getTimeStep());
		Matrix timeH = globalH.add(cdt);

		exportChart(getT0(), 0);

		double endTime = GlobalData.getSimulationTime();
		double timeStep = GlobalData.getTimeStep();
		for (double currentTime = timeStep; currentTime <= endTime; currentTime += timeStep) {
			System.out.println("solver(): currentTime=" + currentTime);

			Matrix timeP = (cdt.multiply(getT0())).add(globalP);
			Matrix t1 = Matrix.gaussianSolve(timeH, timeP);
			updateNodesTemperature(t1);
			exportChart(t1, currentTime);
		}
	}

	public static void exportChart(Matrix temp, double time) {
		(new Thread(() -> {
			try {
				double[][] data = temp
						.changeDimensions(GlobalData.wNodesCount(), GlobalData.hNodesCount())
						.transpose()
						.verticalFlip()
						.copyData();

				HeatChart map = new HeatChart(
						data,
						Math.min(GlobalData.getInitialTemp(), GlobalData.getAmbientTemp()),
						Math.max(GlobalData.getInitialTemp(), GlobalData.getAmbientTemp()));

				map.setTitle("time= " + time + "[s]");
				map.setXAxisLabel("Max: " + Utils.roundDouble(temp.max(), 2) + "     Min: " + Utils.roundDouble(temp.min(), 2));

				map.setXValues(getXAxisValues());
				map.setYValues(getYAxisValues());
				map.setCellSize(new Dimension(8, 8));
				map.setXAxisValuesFrequency(2);
				map.setYAxisValuesFrequency(2);

				File dir = new File(System.getProperty("user.dir") + "\\rendered\\");
				if (!dir.exists()) {
					dir.mkdir();
				}

				map.saveToFile(new File(System.getProperty("user.dir") + "\\rendered\\" + "t_" + time + ".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		})).start();

	}

	public static void computeGlobalHCP() throws ExecutionException, InterruptedException {
		long t1 = System.currentTimeMillis();

		ExecutorService es = Executors.newFixedThreadPool(4);

		Callable<Matrix> c = () -> {
			double tmpC[][] = new double[nodes.size()][nodes.size()];
			int elementsCount = elements.size();

			Element element;
			for (int elementIndex = 0; elementIndex < elementsCount; elementIndex++) {
				element = elements.get(elementIndex);

				for (int i = 0; i < 4; i++) { //Po węzłach elementu
					for (int j = 0; j < 4; j++) {  // Po węzłach elementu
						tmpC[element.id[i]][element.id[j]] += element.getCSum().get(i, j);
					}
				}
			}

			System.out.println("Matrix C ready");
			return new Matrix(tmpC);
		};
		Callable<Matrix> h = () -> {
			double tmpH[][] = new double[nodes.size()][nodes.size()];
			int elementsCount = elements.size();

			Element element;
			for (int elementIndex = 0; elementIndex < elementsCount; elementIndex++) {
				element = elements.get(elementIndex);

				for (int i = 0; i < 4; i++) { //Po węzłach elementu
					for (int j = 0; j < 4; j++) {  // Po węzłach elementu
						tmpH[element.id[i]][element.id[j]] += element.getHSum().get(i, j);
					}
				}
			}

			System.out.println("Matrix H ready");
			return new Matrix(tmpH);
		};
		Callable<Matrix> p = () -> {
			double tmpP[] = new double[nodes.size()];
			for (Element element : elements) {

				for (int i = 0; i < 4; i++) { //Po węzłach elementu
					tmpP[element.id[i]] += element.getPSum().get(i, 0);
				}
			}

			System.out.println("Matrix P ready");
			return new Matrix(tmpP, nodes.size(), 1);
		};
		Runnable clearCachedCH = () -> {
			int elementsCount = elements.size();

			for (int elementIndex = 0; elementIndex < elementsCount; elementIndex++) {
				elements.get(elementIndex).clearCache();
			}
		};

		Future<Matrix> futureC = es.submit(c);
		Future<Matrix> futureH = es.submit(h);
		Future<Matrix> futureP = es.submit(p);

		globalC = futureC.get();
		globalH = futureH.get();
		globalP = futureP.get();

		es.submit(clearCachedCH);
		es.shutdown();
		System.gc();

		long t2 = System.currentTimeMillis();

		double tDelta = (t2 - t1) / 1000.0;

		System.out.println("Computing global H C P finished, time=" + tDelta);
	}

	public static void updateNodesTemperature(Matrix t1) {
		if (t1.entriesCount() != nodes.size()) {
			System.out.println("ERRRRRROR");
		}

		for (int i = 0; i < nodes.size(); i++) {
			nodes.get(i).setTemperature(t1.get(i, 0));
		}
	}

	private static void init() {
		System.out.println("init()");
		GlobalData.setHeight(0.07); //m
		GlobalData.setWidth(0.12); //m
		GlobalData.sethNodesCount(56); //35
		GlobalData.setwNodesCount(96); //60
		GlobalData.setSimulationTime(6000);//s
		GlobalData.setTimeStep(60);//s
		GlobalData.setInitialTemp(975);//'C
		GlobalData.setAmbientTemp(30);//'C
		GlobalData.setAlfa(300);//W/m2k
		GlobalData.setSpecificHeat(900);//J/(kg * C)
		GlobalData.setConductivity(0.8);//W/(m*C)
		GlobalData.setDensity(2165);//km/m3

		for (int i = 0; i < GlobalData.wNodesCount(); i++) {
			for (int j = 0; j < GlobalData.hNodesCount(); j++) {
				double x = GlobalData.elementWidth() * i;
				double y = GlobalData.elementHeight() * j;

				boolean isOnLeftBorder = i == 0;
				boolean isOnRightBorder = i == GlobalData.wNodesCount() - 1;
				boolean isOnTopBorder = j == GlobalData.hNodesCount() - 1;
				boolean isOnBottomBorder = j == 0;
				boolean isOnBorder = isOnLeftBorder || isOnRightBorder || isOnTopBorder || isOnBottomBorder;
				nodes.add(new Node(x, y, isOnLeftBorder || isOnTopBorder || isOnRightBorder));
			}
		}

		for (int i = 0; i < GlobalData.wNodesCount() - 1; i++) {
			for (int j = 0; j < GlobalData.hNodesCount() - 1; j++) {
				int dln = i * GlobalData.hNodesCount() + j; //dolny lewy

				Element e = new Element(new int[]{dln, dln + GlobalData.hNodesCount(), dln + GlobalData.hNodesCount() + 1, dln + 1});

				//e.id[0] = dln;
				//e.id[1] = dln + GlobalData.nB;
				//e.id[2] = dln + GlobalData.nB + 1;
				//e.id[3] = dln + 1;

				elements.add(e);
			}
		}

		try {
			computeGlobalHCP();
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static String[] getXAxisValues() {
		String axisValues[] = new String[GlobalData.wNodesCount()];

		double step = GlobalData.elementWidth();

		for (int i = 0; i < axisValues.length; i++) {
			axisValues[i] = String.valueOf(Utils.roundDouble(step * i, 3));
		}

		return axisValues;
	}

	private static String[] getYAxisValues() {
		String axisValues[] = new String[GlobalData.hNodesCount()];

		double step = GlobalData.elementHeight();

		for (int i = 0; i < axisValues.length; i++) {
			axisValues[i] = String.valueOf(Utils.roundDouble(step * i, 3));
		}

		return axisValues;
	}
}
