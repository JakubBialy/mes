package cc.bialy;

public class Point {
	public double e, n;

	public double getE() {
		return e;
	}

	public void setE(double e) {
		this.e = e;
	}

	public double getN() {
		return n;
	}

	public void setN(double n) {
		this.n = n;
	}

	public Point(double e, double n) {
		this.e = e;
		this.n = n;
	}
}
