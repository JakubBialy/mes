package cc.bialy;

public class Jacobian {
	private double data[][] = new double[2][2];

	public Jacobian(double[][] data) {
		this.data = data;
	}

	public Jacobian(double... data) {
		this.data[0][0] = data[0];
		this.data[0][1] = data[1];
		this.data[1][0] = data[2];
		this.data[1][1] = data[3];
	}

	double get(int i, int j) {
		return data[i][j];
	}

	public Jacobian getInverted() {
		double j_0_0 = (1 / det()) * data[1][1];
		double j_0_1 = (1 / det()) * data[0][1] * (-1);
		double j_1_0 = (1 / det()) * data[1][0] * (-1);
		double j_1_1 = (1 / det()) * data[0][0];

		return new Jacobian(j_0_0, j_0_1, j_1_0, j_1_1);
	}

	public double det() {
		return data[0][0] * data[1][1] - data[0][1] * data[1][0];
	}

	private double roundDouble(double input) {
		return Math.round(input * 100.0) / 100.0;
	}

	@Override
	public String toString() {
		return "{[" + roundDouble(data[0][0]) + "], " +
				"[" + roundDouble(data[0][1]) + "], " +
				"[" + roundDouble(data[1][0]) + "], " +
				"[" + roundDouble(data[1][1]) + "]" +
				'}';
	}
}

/*
[[0,0] [0,1]]
[[1,0] [1,1]]

[[0] [1]]
[[2] [3]]
 */
