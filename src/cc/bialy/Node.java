package cc.bialy;

public class Node {
	public double x;
	public double y;
	private double temperature;
	private boolean status;

	public Node(double x, double y, boolean status) {
		this.x = x;
		this.y = y;
		this.temperature = GlobalData.getInitialTemp();
		this.status = status;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public boolean isStatus() {
		return status;
	}
}
