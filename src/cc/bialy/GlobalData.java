package cc.bialy;

public class GlobalData {
	private static double initialTemp; // Temp początkowa
	private static double timeStep; // Krok czasowy
	private static double ambientTemp; // Temp otoczenia
	private static double simulationTime; // Czas trwania całego procesu
	private static double height; // Wysokość przekroju
	private static double width; // Szerokość przekroju
	private static double alfa;
	private static double specificHeat;
	private static double conductivity;
	private static double density;
	private static int hNodesCount; //Liczba węzłów po wysokości
	private static int wNodesCount; //Liczba węzłów po szerokości

	public static double getInitialTemp() {
		return initialTemp;
	}

	public static void setInitialTemp(double initialTemp) {
		GlobalData.initialTemp = initialTemp;
	}

	public static double getTimeStep() {
		return timeStep;
	}

	public static void setTimeStep(double timeStep) {
		GlobalData.timeStep = timeStep;
	}

	public static double getAmbientTemp() {
		return ambientTemp;
	}

	public static void setAmbientTemp(double ambientTemp) {
		GlobalData.ambientTemp = ambientTemp;
	}

	public static double getSimulationTime() {
		return simulationTime;
	}

	public static void setSimulationTime(double simulationTime) {
		GlobalData.simulationTime = simulationTime;
	}

	public static double getHeight() {
		return height;
	}

	public static void setHeight(double height) {
		GlobalData.height = height;
	}

	public static double getWidth() {
		return width;
	}

	public static void setWidth(double width) {
		GlobalData.width = width;
	}

	public static double getAlfa() {
		return alfa;
	}

	public static void setAlfa(double alfa) {
		GlobalData.alfa = alfa;
	}

	public static double getSpecificHeat() {
		return specificHeat;
	}

	public static void setSpecificHeat(double specificHeat) {
		GlobalData.specificHeat = specificHeat;
	}

	public static double getConductivity() {
		return conductivity;
	}

	public static void setConductivity(double conductivity) {
		GlobalData.conductivity = conductivity;
	}

	public static double getDensity() {
		return density;
	}

	public static void setDensity(double density) {
		GlobalData.density = density;
	}

	public static int gethNodesCount() {
		return hNodesCount;
	}

	public static void sethNodesCount(int hNodesCount) {
		GlobalData.hNodesCount = hNodesCount;
	}

	public static int getwNodesCount() {
		return wNodesCount;
	}

	public static void setwNodesCount(int wNodesCount) {
		GlobalData.wNodesCount = wNodesCount;
	}

	public static int hNodesCount() {return hNodesCount;}

	public static int wNodesCount() {return wNodesCount;}

	public static double elementHeight() { return height / (hNodesCount - 1.0);}

	public static double elementWidth() {return width / (wNodesCount - 1.0);}

	public static int nodesCount() {return hNodesCount * wNodesCount;}
}
