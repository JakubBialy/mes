package cc.bialy;

import java.util.ArrayList;
import java.util.HashMap;

public class Element {
	public static final Point[] PC = new Point[]{
			new Point(-0.577, -0.577), //0
			new Point(0.577, -0.577),  //1
			new Point(0.577, 0.577),   //2
			new Point(-0.577, 0.577)}; //3

	private static final Point[] LOCAL_POINTS = new Point[]{
			new Point(-1, -1),
			new Point(1, -1),
			new Point(1, 1),
			new Point(-1, 1)
	};

	private final HashMap<Point, Jacobian> jacobianForPC = new HashMap<>(4);
	public int[] id = new int[4]; //global node id
	//public int[] walls = new int[4];
	private Matrix cachedHSum = null;
	private Matrix cachedCSum = null;

	public Element(int[] id) {
		this.id = id;

		computeJacobians();
		Matrix sumC = Matrix.fill(0, 4, 4); //liczba f. kształtu
		Matrix sumH = Matrix.fill(0, 4, 4); //liczba f. kształtu

		for (Point pc : PC) {
			sumC = sumC.add(getMatrixCForPC(pc));
			sumH = sumH.add(getMatrixHForPC(pc));
		}

		cachedCSum = sumC.multiply(GlobalData.getSpecificHeat() * GlobalData.getDensity());
		cachedHSum = (sumH.multiply(GlobalData.getConductivity())).add(getKonwekcjaSum());
	}

	public static double Ni_de(int i, Point pc) {
		double result = 0;
		switch (i) {
			case 1:
				result = -0.25 * (1 - pc.n);
				break;
			case 2:
				result = 0.25 * (1 - pc.n);
				break;
			case 3:
				result = 0.25 * (1 + pc.n);
				break;
			case 4:
				result = -0.25 * (1 + pc.n);
				break;
		}

		return result;
	}

	public static double Ni_dn(int i, Point pc) {
		double result = 0;
		switch (i) {
			case 1:
				result = -0.25 * (1 - pc.e);
				break;
			case 2:
				result = -0.25 * (1 + pc.e);
				break;
			case 3:
				result = 0.25 * (1 + pc.e);
				break;
			case 4:
				result = 0.25 * (1 - pc.e);
				break;
		}

		return result;
	}

	public static double Ni(int i, Point pc) {
		double result = 0;
		switch (i) {
			case 1:
				result = 0.25 * (1 - pc.e) * (1 - pc.n);
				break;
			case 2:
				result = 0.25 * (1 + pc.e) * (1 - pc.n);
				break;
			case 3:
				result = 0.25 * (1 + pc.e) * (1 + pc.n);
				break;
			case 4:
				result = 0.25 * (1 - pc.e) * (1 + pc.n);
				break;
		}

		return result;
	}

	public void clearCache(){
		cachedCSum = null;
		cachedHSum = null;
	}

	public Matrix getAllN(Point pc) {
		return new Matrix(new double[]{
				Element.Ni(1, pc),
				Element.Ni(2, pc),
				Element.Ni(3, pc),
				Element.Ni(4, pc)},
				4);
	}

	public Matrix getAllNdx(Point pc) {
		return new Matrix(new double[]{
				Ni_dx(1, pc),
				Ni_dx(2, pc),
				Ni_dx(3, pc),
				Ni_dx(4, pc)},
				4);
	}

	public Matrix getAllNdy(Point pc) {
		return new Matrix(new double[]{
				Ni_dy(1, pc),
				Ni_dy(2, pc),
				Ni_dy(3, pc),
				Ni_dy(4, pc)},
				4);
	}

	public Matrix getMatrixCForPC(Point pc) {
		return getAllN(pc).multiply(getAllN(pc).transpose())        // (N * N^T) *
				.multiply((jacobianForPC.get(pc).det()) * 1 * 1);   // * (det(J) * 1 * 1)
	}

	public Matrix getMatrixHForPC(Point pc) {
		return (getAllNdx(pc).multiply(getAllNdx(pc).transpose()).add((getAllNdy(pc).multiply(getAllNdy(pc).transpose())))  // ((Ndx * Ndx^T) + (Ndy * Ndy^T))*
				.multiply(jacobianForPC.get(pc).det() * 1 * 1));// * (det(J) * 1 * 1)

	}

	public Matrix getKonwekcja(Point pc) { // Macierz P liczymy po powierzchni
		return getAllN(pc).multiply(getAllN(pc).transpose()).multiply((elementSideLengthInGlobal() / 2) * GlobalData.getAlfa());
	}

	private double elementSideLengthInGlobal(){
		return Math.abs(GlobalData.getWidth() / (GlobalData.wNodesCount() - 1));
	}

	public Matrix getMatrixPForPc(Point pc) { // Macierz P liczymy po powierzchni
		return getAllN(pc).multiply(GlobalData.getAlfa() * GlobalData.getAmbientTemp() * (elementSideLengthInGlobal() / 2));
	}

	public Matrix getCSum() {
		if (cachedCSum == null) {
			Matrix sumC = Matrix.fill(0, 4, 4); //liczba f. kształtu

			for (Point pc : PC) {
				sumC = sumC.add(getMatrixCForPC(pc));
			}

			cachedCSum = sumC.multiply(GlobalData.getSpecificHeat() * GlobalData.getDensity());
		}

		return cachedCSum;
	}

	public Matrix getHSum() {
		if (cachedHSum == null) {
			Matrix sumH = Matrix.fill(0, 4, 4); //liczba f. kształtu

			for (Point pc : PC) {
				sumH = sumH.add(getMatrixHForPC(pc));
			}

			cachedHSum = (sumH.multiply(GlobalData.getConductivity())).add(getKonwekcjaSum());
		}
		return cachedHSum;
	}

	public Matrix getKonwekcjaSum(){
		Matrix sumKonwekcja = Matrix.fill(0, 4, 4); //liczba f. kształtu

		for (Point pc : getPCforWalls(getWallsWithStatus())) {
			sumKonwekcja = sumKonwekcja.add(getKonwekcja(pc));
		}

		return sumKonwekcja;
	}

	public Matrix getPSum() {
		Matrix sumP = Matrix.fill(0, 4, 1); //liczba f. kształtu

		for (Point pc : getPCforWalls(getWallsWithStatus())) {
			sumP = sumP.add(getMatrixPForPc(pc));
		}

		return sumP;
	}

	public ArrayList<Pair<Integer>> getWallsWithStatus() {
		ArrayList<Pair<Integer>> result = new ArrayList<>(4);

		for (int i = 0; i < id.length; i++) {
			if (Main.nodes.get(id[i]).isStatus() && Main.nodes.get(id[(i + 1) % id.length]).isStatus()) {
				result.add(new Pair<>(i, (i + 1) % id.length));
			}
		}

		return result;
	}

	public ArrayList<Point> getPCforWalls(ArrayList<Pair<Integer>> walls) {
		ArrayList<Point> result = new ArrayList<>();

		for (Pair<Integer> pair : walls) {
			Point p1 = LOCAL_POINTS[pair.getFirst()];
			Point p2 = LOCAL_POINTS[pair.getSecond()];

			if (p1.n == p2.n) { //Dwa punkty poziomo
				result.add(new Point(-0.577, p1.n));
				result.add(new Point(0.577, p1.n));
			} else { // Dwa punkty pionowo
				result.add(new Point(p1.e, -0.577));
				result.add(new Point(p1.e, 0.577));
			}
		}

		return result;
	}

	public double Ni_dx(int i, Point pc) {
		return Ni_de(i, pc) * jacobianForPC.get(pc).getInverted().get(0, 0) +
				Ni_dn(i, pc) * jacobianForPC.get(pc).getInverted().get(0, 1);
	}

	public double Ni_dy(int i, Point pc) {
		return Ni_de(i, pc) * jacobianForPC.get(pc).getInverted().get(1, 0) +
				Ni_dn(i, pc) * jacobianForPC.get(pc).getInverted().get(1, 1);
	}

	/*
	Każdy element posiada 4 punkty całkowania, dla każdego punktu liczymy jakobian
	 */

	private void computeJacobians() {
		//jacobians = new ArrayList<>(4);
		for (int i = 0; i < id.length; i++) {
			double j_0_0 = Ni_de(1, PC[i]) * Main.nodes.get(this.id[0]).x +
					Ni_de(2, PC[i]) * Main.nodes.get(this.id[1]).x +
					Ni_de(3, PC[i]) * Main.nodes.get(this.id[2]).x +
					Ni_de(4, PC[i]) * Main.nodes.get(this.id[3]).x;

			double j_0_1 = Ni_de(1, PC[i]) * Main.nodes.get(id[0]).y +
					Ni_de(2, PC[i]) * Main.nodes.get(id[1]).y +
					Ni_de(3, PC[i]) * Main.nodes.get(id[2]).y +
					Ni_de(4, PC[i]) * Main.nodes.get(id[3]).y;

			double j_1_0 = Ni_dn(1, PC[i]) * Main.nodes.get(id[0]).x +
					Ni_dn(2, PC[i]) * Main.nodes.get(id[1]).x +
					Ni_dn(3, PC[i]) * Main.nodes.get(id[2]).x +
					Ni_dn(4, PC[i]) * Main.nodes.get(id[3]).x;

			double j_1_1 = Ni_dn(1, PC[i]) * Main.nodes.get(id[0]).y +
					Ni_dn(2, PC[i]) * Main.nodes.get(id[1]).y +
					Ni_dn(3, PC[i]) * Main.nodes.get(id[2]).y +
					Ni_dn(4, PC[i]) * Main.nodes.get(id[3]).y;

			//jacobians.add(new Jacobian(j_0_0, j_0_1, j_1_0, j_1_1));
			jacobianForPC.put(PC[i], new Jacobian(j_0_0, j_0_1, j_1_0, j_1_1));
		}
	}
}